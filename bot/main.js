const { Telegraf } = require('telegraf')
const { download, getFileName, sleep, removeFile } = require('./utils')

let tasks = []

const bot = new Telegraf(process.env.TOKEN)
bot.start((ctx) => ctx.reply('Welcome'))

bot.on('message', async (ctx) => {
    tasks.push(ctx)
    ctx.reply(`Your song is ${tasks.length} on the queue`)
})

bot.launch()

async function main() {
    while (true) {
        await sleep(5000)
        if (tasks.length > 0) {
            await runTask(tasks[0])
            tasks.shift()
        }
    }
}

function runTask(ctx) {
    return new Promise(async (resolve, reject) => {
        const messageData = ctx.update.message.text.split('https')
        let url = null

        for (let i = 0; i < messageData.length; i++) {
            const element = messageData[i];
            if (element.indexOf('://') > -1) {
                url = 'https' + messageData[i]
            }
        }

        if (url === null) {
            ctx.reply(`Wrong command. You can see instructions with help command.`)
            resolve()
        }

        try {
            const fileName = await getFileName(url)

            ctx.reply(`Downloading ${fileName}. It could take a few minutes.`)

            await download(url)

            ctx.reply(`Your song has been downloaded, I'm going to send it to you now. Let me a second...`)

            await sleep()

            await ctx.telegram.sendDocument(ctx.from.id, {
                source: '/downloads/' + fileName,
                filename: fileName
            })

            removeFile('/downloads/' + fileName)
            resolve()
        } catch (error) {
            console.log('error', error)
            ctx.reply(`Error downloading song`)
            reject()
        }
    })

}
main()

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))
