var fs = require('fs')
var path = require('path')

const { spawn } = require("child_process");

const download = (url) => {
    return new Promise((resolve, reject) => {
        const params = ["-x", "-r", "1M", "--audio-format", "mp3", "--audio-quality", "0", "--output", "/downloads/%(uploader)s%(title)s.%(ext)s", url]
        const command = spawn("youtube-dl", params);

        command.stdout.on("data", data => {
            console.log(`download stdout: ${data}`);
        });

        command.stderr.on("data", data => {
            console.log(`stderr: ${data}`);
            // reject()
        });

        command.on('error', (error) => {
            // console.log(`error: ${error.message}`);
            reject(error)
        });

        command.on("close", code => {
            // console.log(`child process exited with code ${code}`);
            if (code === 0) {
                resolve()
            } else {
                reject('exit code ' + code)
            }
        });
    })
}

const getFileName = (url) => {
    // youtube-dl   "" 
    return new Promise((resolve, reject) => {
        const params = ["--get-filename", "-o", "%(uploader)s%(title)s", url]
        const command = spawn("youtube-dl", params);

        command.stdout.on("data", data => {
            console.log(`getFileName stdout: ${data}`);
            const aux = data + '.mp3'
            const aux2 = aux.replace(/(\r\n|\n|\r)/gm, "")
            console.log('aux', aux2)

            resolve(aux2)
        });

        command.stderr.on("data", data => {
            console.log(`stderr: ${data}`);
            // reject()
        });

        command.on('error', (error) => {
            console.log(`error: ${error.message}`);
            reject()
        });

        command.on("close", code => {
            console.log(`child process exited with code ${code}`);
        });
    })
}

const sleep = (timeout = 1000) => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, timeout);
    })
}

const removeFile = (path) => {
    fs.unlinkSync(path);

}
module.exports = { download, getFileName, sleep, removeFile }
