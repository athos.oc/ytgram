FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ="Europe/Madrid"
RUN apt update && apt upgrade -y
# RUN apt install aptitude vim -y
RUN apt install -y tzdata ffmpeg python2.7 curl
RUN ln -s /usr/bin/python2.7 /usr/bin/python
RUN curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
RUN chmod a+rx /usr/local/bin/youtube-dl
RUN apt install nodejs npm -y
RUN mkdir /downloads
RUN npm cache clean -f && npm install -g n && n 16.13.0
RUN npm install pm2 -g
ADD ./bot /bot
WORKDIR /bot
RUN npm i
CMD ["pm2-runtime","ecosystem.config.js"]
